# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Version: silownia 1.0.0

### Steps required to run UI part of application ###

1. Clone repository from -> https://bitbucket.org/MorenP/ssi-silownia
2. Please download Node.js -> 
https://nodejs.org/en/download/
3. Install Node.js and npm (npm is included in Node.js downloaded file)
4. After you install all required software please go to the ssi-silownia-ui folder 
and run npm start command
5. If you see information "webpack: Compiled successfully" you can run you browser on -> http://localhost:4200


### MOCK server ###

https://github.com/dreamhead/moco