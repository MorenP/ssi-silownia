export class Page {
  name: string;
  mainPage: boolean;
  loginPage: boolean;
  registationPage: boolean;

  constructor() {
    this.setMainPage();
  }

  setMainPage() {
    this.loginPage = false;
    this.registationPage = false;
    this.mainPage = true;
  }

  setRegistrationPage() {
    this.mainPage = false;
    this.loginPage = false;
    this.registationPage = true;
  }

  setLoginPage() {
    this.mainPage = false;
    this.registationPage = false;
    this.loginPage = true;
  }

  setPage(name: string) {
    switch(name) {
      case "main_page": {
        this.setMainPage();
        break;
      }
      case "registration_page": {
        this.setRegistrationPage();
        break;
      }
      case "login_page": {
        this.setLoginPage();
        break;
      }
    }
  }
}
