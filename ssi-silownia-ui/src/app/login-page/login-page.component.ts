import {Component, Input} from "@angular/core";
import {Page} from "../../shared/model/page";

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styles: []
})
export class LoginComponent {

  @Input() page: Page;

  constructor() {
  }

  public setPage(name: string) {
    this.page.setPage(name);
  }
}
