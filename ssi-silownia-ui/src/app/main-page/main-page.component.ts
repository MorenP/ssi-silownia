import {Component, Input} from '@angular/core';

import {Page} from "../../shared/model/page";

@Component({
  selector: 'main-page',
  templateUrl: './main-page.component.html',
  styles: []
})
export class MainPageComponent {

  @Input() page: Page;

  constructor() {
  }

  public setPage(name: string) {
    this.page.setPage(name);
  }
}
