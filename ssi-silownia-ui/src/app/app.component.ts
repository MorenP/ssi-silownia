import {Component} from '@angular/core';
import {Page} from "../shared/model/page";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {

  page: Page;

  constructor() {
    this.page = new Page();
  }
}
