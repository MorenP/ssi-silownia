import {Component, Input} from '@angular/core';
import {Page} from "../../shared/model/page";

@Component({
  selector: 'registration-page',
  templateUrl: './registration-page.component.html',
  styles: []
})
export class RegistrationComponent {

  @Input() page: Page;

  constructor() {
  }

  public setPage(name: string) {
    this.page.setPage(name);
  }
}
